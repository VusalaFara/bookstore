FROM openjdk:17
COPY build/libs/bookstore-0.0.1-SNAPSHOT.jar  spring-app.jar
CMD ["java", "-jar", "spring-app.jar"]