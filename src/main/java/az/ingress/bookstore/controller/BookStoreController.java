package az.ingress.bookstore.controller;

import az.ingress.bookstore.model.BookStore;
import az.ingress.bookstore.service.BookStoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequiredArgsConstructor
@RequestMapping("/bookstore")
public class BookStoreController {

    private final BookStoreService service;

    @GetMapping("/{id}")
    public BookStore getBookStoreById(@PathVariable Integer id){

        return service.findById(id).get();
    }

    @GetMapping
    public List<BookStore> getBookStore(){
        return service.getBookStore();

    }

    @PostMapping
    public String saveBookStore(@RequestBody BookStore store){
        service.saveBookStore(store);
        return "added bookstore with name: "+store.getName();
    }
    @PutMapping("/{id}")
    public String updateBookStore(@PathVariable Integer id,@RequestBody BookStore store){
        service.updateBookStore(id, store);
        return "updated bookstore with id: "+id;
    }

    @DeleteMapping("/{id}")
    public String deleteBookStore(@PathVariable Integer id){
        service.deleteBookStore(id);
        return "deleted bookstore with id: "+id;
    }
}
