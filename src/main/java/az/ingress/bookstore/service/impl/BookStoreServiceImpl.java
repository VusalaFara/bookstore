package az.ingress.bookstore.service.impl;
import az.ingress.bookstore.model.BookStore;
import az.ingress.bookstore.repository.BookStoreRepository;
import az.ingress.bookstore.service.BookStoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
@RequiredArgsConstructor
public class BookStoreServiceImpl implements BookStoreService {

    private final BookStoreRepository repository;


    @Override
    public Optional<BookStore> findById(Integer id) {
        return repository.findById(id);
    }

    @Override
    public List<BookStore> getBookStore() {
        return repository.findAll();
    }

    @Override
    public String saveBookStore(BookStore store) {
        repository.save(store);
        return "added";
    }
//        public String updateBookStore(Integer id, BookStore store){
//       Optional<BookStore> bookStore = repository.findById(id);
//            bookStore.get().setName(store.getName());
//            bookStore.get().setAddress(store.getAddress());
//       repository.save(bookStore.get());
//       return "added";
//
//   }
            public String updateBookStore(Integer id, BookStore store) {
                if (repository.findById(id).isPresent()) {
                    BookStore store1 = repository.findById(id).get();
                    store1.setName(store.getName());
                    store1.setAddress(store.getAddress());
                    repository.save(store1);
                } else {
                    throw new RuntimeException("user not found with : " + id);
                }
                return "added";

//    }
//    @Override
//    public String updateBookStore(Integer id, BookStore store) {
//        repository.findById(id).stream().map(store1 -> {
//            store1.setName(store.getName());
//            store1.setAddress(store.getAddress());
//            return repository.save(store1);
//        }).findAny().get();
//        return "added";
//    }
            }

    @Override
    public Integer deleteBookStore(Integer id) {
         repository.deleteById(id);
         return id;
    }
}
