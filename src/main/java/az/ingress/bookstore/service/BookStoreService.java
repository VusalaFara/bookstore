package az.ingress.bookstore.service;

import az.ingress.bookstore.model.BookStore;

import java.util.List;
import java.util.Optional;

public interface BookStoreService {

    public Optional<BookStore> findById(Integer id);
    public List<BookStore> getBookStore();
    public String saveBookStore(BookStore store);
    public String updateBookStore(Integer id, BookStore store);
    public Integer deleteBookStore(Integer id);
}
