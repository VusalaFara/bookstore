package az.ingress.bookstore.repository;

import az.ingress.bookstore.model.BookStore;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookStoreRepository extends JpaRepository<BookStore, Integer> {
}
